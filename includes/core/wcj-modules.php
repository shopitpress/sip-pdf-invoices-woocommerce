<?php
/**
 * Booster for WooCommerce - Modules
 *
 * @version 5.3.1
 * @since   3.2.4
 * @author  Pluggabl LLC.
 */

if ( ! defined( 'ABSPATH' ) ) exit;

$wcj_module_files = array(
	'pdf-invoices/submodules/class-wcj-pdf-invoicing-numbering.php',
	'pdf-invoices/submodules/class-wcj-pdf-invoicing-templates.php',
	'pdf-invoices/submodules/class-wcj-pdf-invoicing-styling.php',
	'pdf-invoices/submodules/class-wcj-pdf-invoicing-header.php',
	'pdf-invoices/submodules/class-wcj-pdf-invoicing-footer.php',
	'pdf-invoices/submodules/class-wcj-pdf-invoicing-page.php',
	'pdf-invoices/submodules/class-wcj-pdf-invoicing-emails.php',
	'pdf-invoices/submodules/class-wcj-pdf-invoicing-display.php',
	'pdf-invoices/submodules/class-wcj-pdf-invoicing-advanced.php',
	'class-wcj-pdf-invoicing.php',
);

$this->modules = array();
$wcj_modules_dir = WCJ_PLUGIN_PATH . '/includes/';
foreach ( $wcj_module_files as $wcj_module_file ) {
	$module = include_once( $wcj_modules_dir . $wcj_module_file );
	$this->modules[ $module->id ] = $module;
}
$this->modules = apply_filters( 'wcj_modules_loaded', $this->modules );