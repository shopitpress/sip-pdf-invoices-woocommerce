<?php
/**
 * 
 * @link              https://shopitpress.com
 *
 * @wordpress-plugin
 * Plugin Name:       SIP PDF Inovices for WooCommerce
 * Plugin URI:        https://shopitpress.com/plugins/
 * Description:       WooCommerce add-on: PDF Invoice for WooCommerce
 * Version:           5.3.5
 * Author:            ShopitPress
 * Author URI:        https://shopitpress.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Copyright:		  © 2015 ShopitPress(email: hello@shopitpress.com)
 * Text Domain:       sip-pdf-invoices-for-woocommerce
 * Domain Path:       /languages
 * WC tested up to:   4.5
 * 
 * 
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

// Core functions
require_once( 'includes/functions/wcj-functions-core.php' );

define( 'SIP_PIW_PLUGIN_NAME', 'SIP PDF Inovices for WooCommerce' );
define( 'SIP_PIW_PLUGIN_SLUG', 'sip-pdf-invoices-for-woocommerce' );
define( 'SIP_PIW_PLUGIN_VERSION', '1.0.8' );
define( 'SIP_PIW_PLUGIN_PURCHASE_URL', 'https://shopitpress.com/plugins/sip-social-proof-woocommerce/' );
define( 'SIP_PIW_BASENAME', plugin_basename( __FILE__ ) );
define( 'SIP_PIW_DIR', trailingslashit( plugin_dir_path( __FILE__ ) ) );
define( 'SIP_PIW_URL', trailingslashit( plugin_dir_url( __FILE__ ) ) );

// Check if WooCommerce is active
if ( ! wcj_is_plugin_activated( 'woocommerce', 'woocommerce.php' ) ) {
	return;
}

// Check if Plus is active
if ( 'woocommerce-jetpack.php' === basename( __FILE__ ) && wcj_is_plugin_activated( 'booster-plus-for-woocommerce', 'booster-plus-for-woocommerce.php' ) ) {
	return;
}

if ( ! defined( 'WCJ_PLUGIN_FILE' ) ) {
	/**
	 * WCJ_PLUGIN_FILE.
	 *
	 * @since 3.2.4
	 */
	define( 'WCJ_PLUGIN_FILE', __FILE__ );
}

if ( ! class_exists( 'WC_Jetpack' ) ) :

/**
 * Main WC_Jetpack Class
 *
 * @class   WC_Jetpack
 * @version 3.2.4
 * @since   1.0.0
 */
final class WC_Jetpack {

	/**
	 * Booster for WooCommerce version.
	 *
	 * @var   string
	 * @since 2.4.7
	 */
	public $version = '5.3.5';

	/**
	 * @var WC_Jetpack The single instance of the class
	 */
	protected static $_instance = null;

	/**
	 * @version 5.3.3
	 * @since   5.3.3
	 *
	 * @var array
	 */
	public $options = array();

	/**
	 * Main WC_Jetpack Instance.
	 *
	 * Ensures only one instance of WC_Jetpack is loaded or can be loaded.
	 *
	 * @version 1.0.0
	 * @since   1.0.0
	 * @static
	 * @see    WCJ()
	 * @return WC_Jetpack - Main instance
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * WC_Jetpack Constructor.
	 *
	 * @version 3.2.4
	 * @since   1.0.0
	 * @access  public
	 */
	function __construct() {
		require_once( 'includes/core/wcj-loader.php' );
	}

}

endif;

if ( ! function_exists( 'WCJ' ) ) {
	/**
	 * Returns the main instance of WC_Jetpack to prevent the need to use globals.
	 *
	 * @version 2.5.7
	 * @since   1.0.0
	 * @return  WC_Jetpack
	 */
	function WCJ() {
		return WC_Jetpack::instance();
	}
}

WCJ();
